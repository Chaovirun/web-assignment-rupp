// on-click listener
$("#btn-show").click(function(e) {
    // AJAX call on button clicked
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://127.0.0.1:8080/api/employee/",
        error: function (xhr) {
            console.log(xhr.statusText);
        }
    }).done(function(data) {
        // render data to html template
        var html = template({employees : data});
        $("#render-here").html(html);
    });
});